package org.example.task;

import java.util.List;
import java.util.Map;

public class MainTask {
    public static void main(String[] args) {

        Author darius = new Author("Darius", "Spiridon", Gender.MALE);
        Author mihai = new Author("Mihai", "Eminescu", Gender.MALE);
        Author mircea = new Author("Mirea", "Eliade", Gender.MALE);

        Book book = new Book("Star of the mounth", 50.0f, 2013, List.of(darius, mircea), Genre.ACTION);
        Book luceafarul = new Book("Luceafarul", 100.0f, 1940, List.of(darius, mircea, mihai), Genre.ADVENTURE);
        Book luna = new Book("Luna", 20.0f, 1960, List.of(darius, mihai), Genre.TEHNIC);

        BookService bookService = new BookService();
        bookService.addBook(book);
        bookService.addBook(luna);
        bookService.addBook(luceafarul);

        System.out.println("Initial Book List...");
        printBooks(bookService.getBooks());

        System.out.println();
        System.out.println("Sorted by title  ascendent...");
        printBooks(bookService.sortedByTitleAscending());

        System.out.println();
        System.out.println("Sorted by title  descendent...");
        printBooks(bookService.sortedByTitleDescending());

        System.out.println();
        System.out.println("Sorted by year of relase...");
        printBooks(bookService.sortByYearOfRealase());

        System.out.println();
        System.out.println("Sorted by price...");
        printBooks(bookService.sortByPrice());

        System.out.println();
        System.out.println("Get all authors whit books");
        for (Map.Entry<Author, List<Book>> entry : bookService.getAuthorWhitBooks().entrySet()) {
            System.out.println(entry.getKey() + " has book: ");
            for (Book book1 : entry.getValue()) {
                System.out.println(book1);
            }
            System.out.println();

        }

        System.out.println();
        System.out.println("Get book by author = Mihai");
        printBooks(bookService.getByAuthor(mihai));

        System.out.println();
        System.out.println("Get book by genre ACTION");
        printBooks(bookService.getByGenre(Genre.ACTION));

        System.out.println();
        System.out.println("Get book by before yeare 1950");
        printBooks(bookService.getBeforeYear(1950));

        System.out.println();
        System.out.println("Is book present Luceafarul");
        System.out.println(bookService.isBookPresent(luceafarul));

        System.out.println();
        System.out.println("Remove luceafarul from the list");
        bookService.removeBook(luceafarul);
        printBooks(bookService.getBooks());

        System.out.println();
        System.out.println("Is book present Luceafarul");
        System.out.println(bookService.isBookPresent(luceafarul));

        System.out.println();
        System.out.println("Get all authors");
        System.out.println(bookService.getAllAuthors());




    }

    public static void printBooks(List<Book> books) {
        for (Book book : books) {
            System.out.println(book);
        }
    }


}
