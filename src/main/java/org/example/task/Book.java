package org.example.task;

import java.util.List;
import java.util.Objects;

public class Book implements Comparable<Book> {
    private String title;
    private float price;
    private int yearOfRelase;
    private List<Author> authors;
    private Genre genere;

    public Book(String title, float price, int yearOfRelase, List<Author> authors, Genre genere) {
        this.title = title;
        this.price = price;
        this.yearOfRelase = yearOfRelase;
        this.authors = authors;
        this.genere = genere;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getYearOfRelase() {
        return yearOfRelase;
    }

    public void setYearOfRelase(int yearOfRelase) {
        this.yearOfRelase = yearOfRelase;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public Genre getGenere() {
        return genere;
    }

    public void setGenere(Genre genere) {
        this.genere = genere;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book book)) return false;
        return Float.compare(book.price, price) == 0 && yearOfRelase == book.yearOfRelase && Objects.equals(title, book.title) && Objects.equals(authors, book.authors) && genere == book.genere;
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, price, yearOfRelase, authors, genere);
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", price=" + price +
                ", yearOfRelase=" + yearOfRelase +
                ", authorList=" + authors +
                ", genere=" + genere +
                '}';
    }

    @Override
    public int compareTo(Book o) {
        return title.compareTo(o.getTitle());
    }
}
