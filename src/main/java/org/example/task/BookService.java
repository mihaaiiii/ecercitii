package org.example.task;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class BookService {

    private List<Book> books;

    public BookService() {

        books = new ArrayList<>();

    }

    public void addBook(Book book) {
        books.add(book);
    }

    public void removeBook(Book book) {
        books.remove(book);
    }

    public List<Book> getBooks() {
        return this.books;
    }

    public List<Book> sortedByTitleAscending() {
        List<Book> sortedByTitle = new ArrayList<>();
        sortedByTitle.addAll(books);
        Collections.sort(sortedByTitle);
        return sortedByTitle;
    }

    public List<Book> sortedByTitleDescending() {
        List<Book> sortedByTitle = new ArrayList<>();
        sortedByTitle.addAll(books);
        Collections.sort(sortedByTitle, Collections.reverseOrder());
        return sortedByTitle;
    }

    public List<Book> getByGenre(Genre genere) {
        List<Book> bookByGenre = new ArrayList<>();
        for (Book book : books) {
            if (book.getGenere() == (genere)) {
                bookByGenre.add(book);
            }
        }
        return bookByGenre;
    }

    public List<Book> getBeforeYear(int year) {
        List<Book> bookBeforeYear = new ArrayList<>();
        for (Book book : books) {
            if (book.getYearOfRelase() <= year) {
                bookBeforeYear.add(book);
            }
        }
        return bookBeforeYear;
    }

    public boolean isBookPresent(Book book) {
        return books.contains(book);
    }

    public List<Book> getByAuthor(Author author) {
        List<Book> booksByAuthor = new ArrayList<>();
        for (Book book : books) {
            if (book.getAuthors().contains(author)) {
                booksByAuthor.add(book);
            }
        }
        return booksByAuthor;
    }

    public List<Book> sortByYearOfRealase() {
        List<Book> sortedAscendent = new ArrayList<>();

        sortedAscendent.addAll(books);

        Collections.sort(sortedAscendent, new Comparator<Book>() {

            @Override
            public int compare(Book o1, Book o2) {
                if (o1.getYearOfRelase() < o2.getYearOfRelase()) {
                    return -1;
                } else if (o1.getYearOfRelase() > o2.getYearOfRelase()) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });

        return sortedAscendent;
    }

    public List<Book> sortByPrice() {
        List<Book> sortedDescendentByPrice = new ArrayList<>();

        sortedDescendentByPrice.addAll(books);

        sortedDescendentByPrice.sort(new Comparator<Book>() {
            @Override
            public int compare(Book o1, Book o2) {
                return Float.valueOf(o1.getPrice()).compareTo(o2.getPrice());
            }
        }.reversed());
        return sortedDescendentByPrice;
    }

    public Set<Author> getAllAuthors() {
        Set<Author> authors = new HashSet<>();
        for (Book book : books) {
            authors.addAll(book.getAuthors());
        }
        return authors;
    }

    public Map<Author, List<Book>> getAuthorWhitBooks() {
        Map<Author, List<Book>> authorWhitBooks = new HashMap<>();
        for (Book book : books) {
            for (Author author : book.getAuthors()) {
                if (authorWhitBooks.containsKey(author)) {
                    authorWhitBooks.get(author).add(book);
                } else {
                    List<Book> authorBooks = new ArrayList<>();
                    authorBooks.add(book);
                    authorWhitBooks.put(author, authorBooks);
                }
            }
        }
        return authorWhitBooks;
    }
}
